//
// Created by pavel on 19.12.15.
//

#include "FenwickTree.h"

const int FenwickTree::INF = 1337;

FenwickTree::FenwickTree(size_t size) {
    setSize(size);
}

FenwickTree::FenwickTree(std::vector<int> data) {
    setSize(data.size());

    for (size_t i = 0; i < data.size(); i++)
        update(i + 1, data[i]);
}

void FenwickTree::setSize(size_t size) {
    int newSize = 1;
    while (newSize < size)
        newSize <<= 1;
    left = std::vector<int>(newSize, INF);
    right = std::vector<int>(newSize, INF);
    data = std::vector<int>(newSize, INF);
}

int FenwickTree::rmq(int l, int r) const {
    int ans = data[l - 1]; // data в 0-индексации
    int newR = l; // В любой момент времени newR - максимальное число такое что минимум на отрезке [l, newR] уже найден
    while (newR + g(newR) <= r) {
        ans = std::min(ans, right[newR]);
        newR += g(newR);
    }

    // Осталось найти минимум на отрезке [newR + 1, r];
    // Его найдем с помощью массива left
    int newL = r; // В любой момент времени newL - минимальное число такое что минимум на отрезке [newL + 1, r] уже найден
    while (newL > newR) {
        ans = std::min(ans, left[newL]);
        newL -= g(newL);
    }

    return ans;
}

void FenwickTree::update(int index, int newValue) {
    data[index - 1] = newValue;
    for (int i = index; i <= data.size(); i += g(i))
        left[i] = std::min(left[i], newValue);

    for (int i = index - 1; i > 0; i -= g(i))
        right[i] = std::min(right[i], newValue);
}



