//
// Created by pavel on 20.12.15.
//

#include <algorithm>
#include "FenwickTree2D.h"

const int FenwickTree2D::INF = INT32_MAX;

FenwickTree2D::FenwickTree2D(std::vector<std::vector<int> > data) {
    setSize(data.size(), data[0].size());

    std::vector< std::pair<int, std::pair<int, int> > > elements;
    for (size_t i = 0; i < data.size(); i++)
        for (size_t j = 0; j < data[i].size(); j++) {
            this->data[i].update(j + 1, data[i][j]);
            elements.push_back(std::make_pair(data[i][j], std::make_pair(i + 1, j + 1)));
        }
    std::sort(elements.begin(), elements.end());

    for (size_t i = elements.size(); i > 0; i--)
        update(elements[i - 1].second.first, elements[i - 1].second.second, elements[i - 1].first);
}

FenwickTree2D::FenwickTree2D(size_t width, size_t height) {
    setSize(width, height);
}


void FenwickTree2D::setSize(size_t width, size_t height) {
    int newWidth = 1;
    while (newWidth <= width)
        newWidth <<= 1;
    int newHeight = 1;
    while (newHeight <= height)
        newHeight <<= 1;
    left = std::vector<FenwickTree>(newWidth, FenwickTree(newHeight));
    right = std::vector<FenwickTree>(newWidth, FenwickTree(newHeight));
    data = std::vector<FenwickTree>(newWidth, FenwickTree(newHeight));
}

int FenwickTree2D::rmq(int x1, int y1, int x2, int y2) const {
    int ans = data[x1 - 1].rmq(y1, y2);
    int newR = x1;
    while (newR + g(newR) <= x2) {
        ans = std::min(ans, right[newR].rmq(y1, y2));
        newR += g(newR);
    }

    int newL = x2;
    while (newL > newR) {
        ans = std::min(ans, left[newL].rmq(y1, y2));
        newL -= g(newL);
    }

    return ans;
}

void FenwickTree2D::update(int x, int y, int newValue) {
    data[x - 1].update(y, newValue);
    for (int i = x; i < data.size(); i += g(i))
        left[i].update(y, newValue);

    for (int i = x - 1; i > 0; i -= g(i))
        right[i].update(y, newValue);
}

