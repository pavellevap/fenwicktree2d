//
// Created by pavel on 19.12.15.
//

#ifndef FENWICKTREE_FENWICKTREE_H
#define FENWICKTREE_FENWICKTREE_H

#include <glob.h>
#include <vector>
#include <iostream>

class FenwickTree { // В запросах используется 1-индексация
public:
    friend class FenwickTree2D;

    FenwickTree(std::vector<int> data);

    int rmq(int l, int r) const ;

private:
    static const int INF;

    std::vector<int> left; // left[i] содержит минимум на отрезке [i - g(i) + 1, i]
    std::vector<int> right; // right[i] содержит минимум на отрезке [i + 1, i + g(i)]
    std::vector<int> data;

    FenwickTree(size_t size);

    void setSize(size_t size);

    void update(int index, int newValue);

    int g(int x) const { return x & (-x); }
};

#endif //FENWICKTREE_FENWICKTREE_H
