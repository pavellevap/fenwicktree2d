//
// Created by pavel on 19.12.15.
//

#ifndef FENWICKTREE_FENWICKTREE2D_H
#define FENWICKTREE_FENWICKTREE2D_H

#include <glob.h>
#include <vector>
#include <iostream>

#include "FenwickTree.h"

class FenwickTree2D { // В запросах используется 1-индексация
public:
    FenwickTree2D(std::vector< std::vector<int> > data);

    int rmq(int x1, int y1, int x2, int y2) const;

private:
    static const int INF;

    // left[i] содержит дерево фенвика для минимума.
    // Элементами этого дерева являются числа min{data[i - g(i) + 1][j], ... ,data[i][j]]} для j = 1 .. height
    std::vector<FenwickTree> left;

    // right[i] содержит дерево фенвика для минимума.
    // Элементами этого дерева являются числа min{data[i + 1][j], ... ,data[i + g(i)][j]]} для j = 1 .. height
    std::vector<FenwickTree> right;

    // data[i] содержит дерево фенвика для для минимума.
    // Элементами этого дерева являются числа data[i][j] для j = 1 .. height
    std::vector<FenwickTree> data;

    FenwickTree2D(size_t width, size_t height);

    void setSize(size_t width, size_t height);

    void update(int x, int y, int newValue);

    int g(int x) const { return x & (-x); }
};

#endif //FENWICKTREE_FENWICKTREE2D_H
