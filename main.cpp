#include <iostream>
#include "FenwickTree.h"
#include "FenwickTree2D.h"

using namespace std;

int main() {

    vector< vector<int> > data{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    FenwickTree2D tree(data);

    while (true) {
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;
        cout << tree.rmq(x1, y1, x2, y2) << endl;
    }

    return 0;
}